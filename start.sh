#!/bin/bash
app="docker.flask"

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)

# #start minio container
# docker run -d -p 9000:9000 minio/minio server /data

# #Get minio mc client
# wget -nc https://dl.min.io/client/mc/release/linux-amd64/mc
# chmod +x mc
# sudo cp mc /usr/bin
# #Connection string. ToDo - do not hardcode this!
# export MC_HOST_miniolocal=http://minioadmin:minioadmin@localhost:9000
# #Make bucket
# mc mb miniolocal/testbucket
# #Upload file
# mc cp /vagrant/minio/Dratuti.jpg miniolocal/testbucket

cd /vagrant/flask
docker build -t ${app} .
#docker run -d -p 56735:5000 ${app}
sudo apt-get install -y docker-compose 

cd /vagrant/traefik
docker-compose up -d